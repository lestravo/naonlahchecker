Array.prototype.remove = function(value) {
    var index = this.indexOf(value);
    if (index != -1) {
        this.splice(index, 1);
    }
    return this;
};

function enableTextArea(bool) {
    $('#socks').attr('disabled', bool);
    $('#mailpass').attr('disabled', bool);
    $('#token').attr('disabled', bool);
}

function gbrn_liveUp() {
    var count = parseInt($('#acc_live_count').html());
    count++;
    $('#acc_live_count').html(count + '');
}

function gbrn_dieUp() {
    var count = parseInt($('#acc_die_count').html());
    count++;
    $('#acc_die_count').html(count + '');
}

function gbrn_wrongUp() {
    var count = parseInt($('#wrong_count').html());
    count++;
    $('#wrong_count').html(count + '');
}

function gbrn_badUp() {
    var count = parseInt($('#bad_count').html());
    count++;
    $('#bad_count').html(count + '');
}

function stopLoading(bool) {
    $('#loading').attr('src', 'img/clear.gif');
    var str = $('#checkStatus').html();
    $('#checkStatus').html(str.replace('Checking', 'Stopped'));
    enableTextArea(false);
    $('#submit').attr('disabled', false);
    $('#stop').attr('disabled', true);
    if ($('#token').val() == '') {
        $('#modal_Notoken').modal('show');
    } else {
        if (bool) {
            $('#modal_done').modal('show');
        } else {
            ajaxCall.abort();
        }
    }
}

function updateTitle(str) {
    document.title = str;
}

function updateTextBox(mp) {
    var mailpass = $('#mailpass').val().split("\n");
    mailpass.remove(mp);
    $('#mailpass').val(mailpass.join("\n"));
}

function paypalv1(lstMP, curMP, nsid, csrf, no) {

    if (lstMP.length < 1 || curMP >= lstMP.length) {
        stopLoading(true);
        return false;
    }
    updateTextBox(lstMP[curMP]);
    ajaxCall = $.ajax({
        url: '/paypal/' + nsid + '/' + csrf + '/' + lstMP[curMP],
        dataType: 'json',
        cache: false,
        type: 'GET',
        beforeSend: function(e) {
            updateTitle('[' + no + '/' + lstMP.length + '] | On Progress');
            $('#process_status').fadeIn(4000);
            $('#checkStatus').html('Checking: ' + lstMP[curMP]);
        },
        success: function(data) {
            switch (data.error) {
                case 'ghoper':
                    stopLoading(false);
                    ajaxCall.abort();
                    $('#modal_token').modal('show');
                    return false;
                    break;
                case 123:
                    stopLoading(false);
                    ajaxCall.abort();
                    $('#modal_login').modal('show');
                    return false;
                    break;
                case 0:
                    curMP++;
                    $('#acc_live').append(data.msg + '<br />');
                    gbrn_liveUp();
                    no++;
                    break;
                case -1:
                    curMP++;
                    $('#acc_wrong').append(data.msg + '<br />');
                    gbrn_wrongUp();
                    no++;
                    generateToken()
                    var nsid = $('#nsid').val()
                    var csrf = $('#csrf').val()
                    break;
                case 1:
                case 3:
                    $('#acc_bad').append(data.msg + '<br />');
                    gbrn_badUp();
                    break;
                case 2:
                    curMP++;
                    $('#acc_die').append(data.msg + '<br />');
                    no++;
                    gbrn_dieUp();
                    break;
            }
            paypalv1(lstMP, curMP, nsid, csrf, no)
        }
    });
return true;
}

function filterMP(mp){
	var mps = mp.split("\n");
	var filtered = new Array();
	var lstMP = new Array();
	for(var i=0;i<mps.length;i++){
		if(mps[i].indexOf('@')!=-1){
			var infoMP = mps[i].split("|");
			for(var k=0;k<infoMP.length;k++){
				if(infoMP[k].indexOf('@')!=-1){
					var email = $.trim(infoMP[k]);
					if(filtered.indexOf(email.toLowerCase())==-1){
						filtered.push(email.toLowerCase());
						lstMP.push(email);
						break;
					}
				}
			}
		}
	}
	return lstMP;
}

function resetResult() {
    $('#acc_die,#wrong,#acc_bad').html('');
    $('#acc_die_count,#wrong_count,#bad_count').text(0)
}
function generateToken(){
    $.ajax({
        type: "GET",
        url: "/paypal/token",
        cache: false,
        success: function(data) {
            $('#nsid').val(data.nsid)
            $('#csrf').val(data._csrf)
        }
    })
}
$(document).ready(function() {
    $('#stop').attr('disabled', true).click(function() {
        stopLoading(false)
    });
    $('#submit').click(function() {
        var mailpass = filterMP($('#mailpass').val())
        var nsid = $('#nsid').val()
        var csrf = $('#csrf').val()
        if (nsid == '' || csrf == '' ) {
            alert("NSID dan CSRF Tidak Ada")
            return false
        } else if ($('#mailpass').val().trim() == '') {
            alert("Email Tidak Ada")
            return false
        }
        $('#mailpass').val(mailpass.join("\n")).attr('disabled', true)
        $('#submit').attr('disabled', true)
        $('#stop').attr('disabled', false)
        paypalv1(mailpass, 0, nsid, csrf, 1)
        return false
    });
    $('#live_btn-hide').click(function() {
        $('#body_live').toggle(1000)
    });
    $('#die_btn-hide').click(function() {
        $('#body_die').toggle(1000)
    });
    $('#wrong_btn-hide').click(function() {
        $('#body_wrong').toggle(1000)
    });
    $('#socks_btn-hide').click(function() {
        $('#body_socks').toggle(1000)
    });
    generateToken()
})