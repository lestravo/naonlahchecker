'use strict'

const User = use('App/Models/User')
const News = use('App/Models/News')
const Checker = use('App/Models/Checker')
const os = require('os-utils')
const fetch = require('node-fetch');

// platform: platform, cpuCount: cpuCount, processUptime: processUptime, freememPercentage: freememPercentage, totalmem: totalmem, freemem: freemem, sysUptime: sysUptime
class UserController {

	async getIndex ({ auth, view, response }) {
		try {
		  await auth.check()
		  const news = await News.all()
		  const user = await auth.getUser()
		  const checker = await Checker.query().orderBy('name', 'asc').fetch()
		  var platform = os.platform();
		  var cpuCount = os.cpuCount()
		  var processUptime = os.processUptime()
		  var freememPercentage = os.freememPercentage()
		  var totalmem = os.totalmem()
		  var freemem = os.freemem()
		  var sysUptime = os.sysUptime()
		  return view.render('dashboard', { user : user, checker: checker.toJSON(), news: news.toJSON(), platform: platform, cpuCount: cpuCount, processUptime: processUptime, freememPercentage: freememPercentage, totalmem: totalmem, freemem: freemem, sysUptime: sysUptime})
		} catch (e) {
		  const logout = await auth.logout()
		  return view.render('login', {})
		}
	}

	async postLogin ({ request, auth, response, session }) {
		const users = await User.query().where({
		  username: request.input('username'),
		  password:  request.input('password')
		})
		if (users[0] == null) {
			session.flash({ notification: 'Wrong username or password' })
			return response.redirect('/')
		} else {
			const login = await auth.login(users[0])
			session.flash({ notification: 'Welcome back '+users[0]['username'] })
			return response.redirect('/')
		}
	}

	async getLogout ({ auth, response }) {
		const logout = await auth.logout()
		return response.redirect('/')
	}

	async getChecker ({ auth, response }) {
		return response.redirect('/')
	}

	async getCheckerSlug ({ params, auth, response, view }) {
		try {
		  await auth.check()
		  const user = await auth.getUser()
		  const checker = await Checker.findBy('slug', params.slug)
		  var platform = os.platform();
		  var cpuCount = os.cpuCount()
		  var processUptime = os.processUptime()
		  var freememPercentage = os.freememPercentage()
		  var totalmem = os.totalmem()
		  var freemem = os.freemem()
		  var sysUptime = os.sysUptime()
		  return view.render('checker', { user : user , slug: params.slug, checker: checker, platform: platform, cpuCount: cpuCount, processUptime: processUptime, freememPercentage: freememPercentage, totalmem: totalmem, freemem: freemem, sysUptime: sysUptime })
		} catch (e) {
		  return response.redirect('/')
		}
	}
}

module.exports = UserController
