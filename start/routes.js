'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/guides/routing
|
*/

const Route = use('Route')

Route.get('/', 'UserController.getIndex')

Route.on('/register').render('register')
Route.post('/login', 'UserController.postLogin')
Route.get('/logout', 'UserController.getLogout')

Route.get('/c', 'UserController.getChecker')
Route.get('/c/:slug', 'UserController.getCheckerSlug')

Route.get('/paypal/token', 'CheckerController.paypalToken')
Route.get('/paypal/:nsid/:csrf/:email', 'CheckerController.paypal')

Route.get('/sbux/:empas', 'CheckerController.sbux')