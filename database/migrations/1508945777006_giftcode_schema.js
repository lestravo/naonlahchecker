'use strict'

const Schema = use('Schema')

class GiftcodeSchema extends Schema {
  up () {
    this.create('giftcodes', (table) => {
      table.increments()
      table.string('num', 24)
      table.string('author', 21)
      table.integer('amount')
      table.integer('registered')
      table.string('regby', 26)
      table.timestamps()
    })
  }

  down () {
    this.drop('giftcodes')
  }
}

module.exports = GiftcodeSchema