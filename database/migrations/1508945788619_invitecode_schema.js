'use strict'

const Schema = use('Schema')

class InvitecodeSchema extends Schema {
  up () {
    this.create('invitecodes', (table) => {
      table.increments()
      table.string('num', 24)
      table.string('author', 21)
      table.integer('registered')
      table.string('regby', 26)
      table.timestamps()
    })
  }

  down () {
    this.drop('invitecodes')
  }
}

module.exports = InvitecodeSchema