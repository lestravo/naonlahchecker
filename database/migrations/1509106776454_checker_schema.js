'use strict'

const Schema = use('Schema')

class CheckerSchema extends Schema {
  up () {
    this.create('checkers', (table) => {
      table.increments()
	  table.string('name')
	  table.string('slug')
	  table.integer('active')
	  table.integer('price')
	  table.integer('type')
	  table.integer('socks')
      table.timestamps()
    })
  }

  down () {
    this.drop('checkers')
  }
}

module.exports = CheckerSchema
