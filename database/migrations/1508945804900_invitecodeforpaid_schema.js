'use strict'

const Schema = use('Schema')

class InvitecodeforpaidSchema extends Schema {
  up () {
    this.create('invitecodeforpaids', (table) => {
      table.increments()
      table.string('num', 24)
      table.string('author', 21)
      table.integer('registered')
      table.string('regby', 26)
      table.timestamps()
    })
  }

  down () {
    this.drop('invitecodeforpaids')
  }
}

module.exports = InvitecodeforpaidSchema
