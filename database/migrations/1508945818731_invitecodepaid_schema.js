'use strict'

const Schema = use('Schema')

class InvitecodepaidSchema extends Schema {
  up () {
    this.create('invitecodepaids', (table) => {
      table.increments()
      table.string('num', 24)
      table.string('author', 21)
      table.integer('registered')
      table.string('regby', 26)
      table.timestamps()
    })
  }

  down () {
    this.drop('invitecodepaids')
  }
}

module.exports = InvitecodepaidSchema
