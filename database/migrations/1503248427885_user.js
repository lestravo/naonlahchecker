'use strict'

const Schema = use('Schema')

class UserSchema extends Schema {
  up () {
    this.create('users', table => {
      table.increments()
      table.string('username', 80).notNullable().unique()
      table.string('password', 60)
      table.decimal('credits').notNullable()
      table.integer('banned')
      table.integer('admin')
      table.decimal('orders')
      table.string('regby', 31)
      table.string('invitecode', 21)
      table.timestamps()
    })
  }

  down () {
    this.drop('users')
  }
}

module.exports = UserSchema